var debug = false;
var lives = 3;

window.onload = playGame;

function playGame() {
  var canvas = document.getElementById('gameCanvas');
  var cc = canvas.getContext('2d');

  canvas.addEventListener('mousemove', handleMouseMove);

  var animate = true;
  requestAnimationFrame(updateAll);

  /* constants and initializers*/
  var playerColor = 'white';
  var gameMessage = '';

  // bricks
  var brickCols = 10;
  var bricks = [
    'red',
    'orange',
    'yellow',
    'green',
    'blue',
    'purple'
  ].map(color => new Array(brickCols).fill(color));
  var brickWidth = canvas.width / brickCols;
  var brickHeight = brickWidth / 3;
  var bricksTopOffset = 100;
  var bricksCount = brickCols * bricks.length;

  // paddle
  var paddleHeight = 15;
  var paddleWidth = 80;
  var paddleX = 400;
  var paddleY = canvas.height - paddleHeight - 40;

  // ball
  var ballRadius = 8;
  var ballX;
  var ballY;
  var newBall;
  var ballSpeedX;
  var ballSpeedY;
  resetBall();
  setGameMessage('good luck', true);

  // mouse
  var mouseX = 400;
  var mouseY;

  function handleMouseMove(e) {
    var rect = canvas.getBoundingClientRect();
    var rootEl = document.documentElement;

    mouseX = e.clientX - rect.left - rootEl.scrollLeft;
    mouseY = e.clientY - rect.top - rootEl.scrollTop;
  }

  function setGameMessage(message, fade = true) {
    gameMessage = message;
    if (fade) {
      setTimeout(() => gameMessage = '', 800);
    }
  }

  function updateAll() {
    drawAll();
    moveAll();
    requestAnimationFrame(updateAll);
  }

  function moveAll() {
    if (!animate) return;

    moveBall();
    movePaddle();
  }

  function moveBall() {
    if (newBall) {
      ballX = paddleX + paddleWidth / 2;
      ballY = paddleY - ballRadius - 1;
      return;
    }

    var newX = ballX + ballSpeedX;
    var newY = ballY + ballSpeedY;
    var left = newX - ballRadius;
    var right = newX + ballRadius;
    var top = newY - ballRadius;
    var bottom = newY + ballRadius;

    // sides
    if (left < 0 && ballSpeedX < 0 ||
        right > canvas.width && ballSpeedX > 0) {
      ballSpeedX = -ballSpeedX;
    }

    // top
    if (top < 0 && ballSpeedY < 0) {
      ballSpeedY = -ballSpeedY;
    }

    // paddle
    if (newX > paddleX &&
        newX < paddleX + paddleWidth &&
        bottom > paddleY &&
        top < paddleY
      ) {
      newY = paddleY - ballRadius - 1;
      ballSpeedY = -ballSpeedY;

      var paddleCenterX = paddleX + paddleWidth / 2;
      var ballOffCenter = (newX - paddleCenterX) / 40 * 8;
      ballSpeedX = ballOffCenter;
      ballSpeedY = -10 + Math.abs(ballSpeedX);
    }
    // bottom
    else if (bottom > canvas.height) {
      lives--;
      if (lives < 1) {
        lose();
      }
      else {
        resetBall();
        return;
      }
    }

    handleBrickBall();

    ballX = newX;
    ballY = newY;
  }

  function handleBrickBall() {
    var newX = ballX + ballSpeedX;
    var newY = ballY + ballSpeedY;

    var row = Math.floor((newY - bricksTopOffset) / brickHeight);
    var col = Math.floor(newX / brickWidth);

    var brickLeft = col * brickWidth;
    var brickRight = brickLeft + brickWidth;
    var brickTop = row * brickHeight + bricksTopOffset;
    var brickBottom = brickTop + brickHeight;

    if (bricks[row] && bricks[row][col]) {
      bricksCount--;
      if (bricksCount < 1) {
        win();
      }
      playerColor = bricks[row][col];
      bricks[row][col] = false;

      if (ballX < brickRight && ballX > brickLeft) {
        ballSpeedY *= -1;
      }
      if (ballY < brickBottom && ballY > brickTop) {
        ballSpeedX *= -1;
      }
    }
  }

  function resetBall() {
    ballX = paddleX + paddleWidth / 2;
    ballY = paddleY - ballRadius - 1;
    ballSpeedX = 0;
    ballSpeedY = 0;
    newBall = true;

    setTimeout(() => {
      ballSpeedX = 3;
      ballSpeedY = -7;
      newBall = false;
    }, 800)
  }

  function movePaddle() {
    // center paddle on mouse horizontally
    paddleX = mouseX - paddleWidth / 2;

    // restrict paddle to canvas
    if (paddleX < 0) {
      paddleX = 0;
    }
    else if (paddleX + paddleWidth > canvas.width) {
      paddleX = canvas.width - paddleWidth;
    }
  }

  function drawAll() {
    drawBackground();
    drawBall();
    drawPaddle();
    drawBricks();

    debug && drawMouseCoords();
    drawLives();
    drawGameMessage();
  }

  function drawBackground() {
    colorRect(0, 0, canvas.width, canvas.height, 'black');
  }

  function drawBall() {
    colorCircle(ballX, ballY, ballRadius, playerColor);
  }

  function drawPaddle() {
    colorRect(paddleX, paddleY, paddleWidth, paddleHeight, playerColor);
  }

  function drawBricks() {
    bricks.forEach((row, y) => {
      row.forEach((color, x) => {
        if (color) {
          colorRect(
            x * brickWidth,
            y * brickHeight + bricksTopOffset,
            brickWidth - 1,
            brickHeight - 1,
            color
          );
        }
      });
    });
  }

  function drawMouseCoords() {
    colorText('mouseX - ' + mouseX, 20, 20);
    colorText('mouseY - ' + mouseY, 20, 30);
  }

  function drawLives() {
    colorText('lives - ' + lives, canvas.width - 50, canvas.height - 10)
  }

  function drawGameMessage() {
    colorText(gameMessage, canvas.width / 2 - 30, canvas.height / 2 + 20, 24);
  }

  function win() {
    setGameMessage('you win', false);
    reload();
  }

  function lose() {
    setGameMessage('you lose', false);
    reload();
  }

  function reload() {
    animate = false;
    setTimeout(() => document.location.reload(), 1000);
  }

  function colorRect(x, y, width, height, color = 'white') {
    cc.fillStyle = color;
    cc.fillRect(x, y, width, height);
  }

  function colorCircle(x, y, radius, color = 'white') {
    cc.fillStyle = color;
    cc.beginPath();
    cc.arc(x, y, radius, 0, Math.PI * 2, true);
    cc.fill();
  }

  function colorText(text, x, y, size = 12, color = 'white') {
    cc.font = size + 'px sans-serif';
    cc.fillStyle = color;
    cc.fillText(text, x, y);
  }
}
